import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const commentSchema = new Schema({
        
    userId:{
        type : Schema.Types.ObjectId,
        ref : 'user'
    },
    message :{
        type : String,
        required : true
    },
    blog_id : {
        type : Schema.Types.ObjectId,
        ref : 'blogger'
    },
    vote:{
        type : Number,
        default: 0
    },
    spammer_from : [{
        type : Schema.Types.ObjectId,
        ref : 'blogger'
    }],
    is_spammer : {
        type: Boolean,
        default : false
    },
    children :[{
        type : Schema.Types.ObjectId,
        ref : 'Comment'
    }]
});

export default mongoose.model('Comment' , commentSchema);