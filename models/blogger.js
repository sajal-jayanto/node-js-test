import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const bloggerSchema = new Schema({
    
    userId:{
        type : Schema.Types.ObjectId,
        ref : 'user'
    },
    content: {
        type : String,
        required : true
    },
    can_comment: {
        type : Boolean,
        default : true
    },
    comment:[{
        type : Schema.Types.ObjectId,
        ref : 'comment'
    }]
    
});

export default mongoose.model('blogger' , bloggerSchema);