import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
        
    userName:{
        type: String,
        required : true
    },
    email:{
        type: String,
        unique: true ,
        required : true
    },
    role:{
        type : Object,
        default: {
            roleId : 1,
            roleName : "commenter"
        }
    },
    password:{
        type: String,
        required : true
    },
    is_active:{
        type : Boolean,
        default: true
    }
});

export default mongoose.model('User' , UserSchema);