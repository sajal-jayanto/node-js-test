import jwt from 'jsonwebtoken';

import dotenv from 'dotenv';

dotenv.config();

export const authorize = (req , res , next) => {

    const token = req.header('token');
    if(!token) {
        return res.status(401).json({
            message : "Login First"
        });
    }
    
    jwt.verify(token , process.env.TOKEN_SECRET , (err , decoded) => {
        if(err){
            return res.status(401).json({
                message : "Access Deniad"
            });
        }
        req.userId = decoded;
        next();
    });
}