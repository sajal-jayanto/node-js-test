import express from 'express';
import {createUser , userProfile , userLogin } from '../controller/user.js';


const router = express.Router();


router.post('/register' , createUser);
router.get('/profile/:id' , userProfile);
router.post('/login' , userLogin);


export default router;