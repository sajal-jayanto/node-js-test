import express from 'express';
import { createComment , createVote , editComment } from '../controller/comment.js';
import { authorize } from './authorizebytoken.js';


const router = express.Router();

router.post('/create' , authorize , createComment);
router.post('/vote/:id/:value' , authorize , createVote);
router.post('/edit/:id' , authorize , editComment);


export default router;