import express from 'express';
import { creatBloge } from '../controller/blogger.js';
import { authorize } from './authorizebytoken.js';

const router = express.Router();

router.post('/create' , authorize , creatBloge);



export default router;