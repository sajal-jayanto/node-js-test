import dotenv from 'dotenv';

dotenv.config();


export const connect_mongodb = async ( mongoose ) => {

    try {

        let connectOption = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        };

        await mongoose.connect(process.env.DB_CONNECTION , { ...connectOption } );
        console.log("Connected... to local dataBase");


    } catch (err){
        console.error(err);
    }
};



export const disconnect_mongodb = async ( mongoose ) => {

    try {
        return await mongoose.disconnect();
    } catch(err) {
        return err;
    }
};
