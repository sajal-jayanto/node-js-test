import Blogger  from '../models/blogger.js';


export const creatBloge = async (req , res) => {

    const { message } = req.body;

    try {    
        const userId = req.userId._id;

        const savebloge = new Blogger({
            userId : userId,
            content : message,
            children : []
        });

        const saveData = await savebloge.save();
        const saveInfo = saveData._doc;
        
        return res.status(200).json({ 
            status : true,
            blogge : saveInfo,
            message : `comment posted successfully`
        });
    }
    catch(err){
        return res.status(400).json({
                
            status : false,
            message : err

        });
    }
}