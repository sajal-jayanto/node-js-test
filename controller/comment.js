import Comment from '../models/comment.js';


export const createComment = async (req , res) => {

    const { message , blogId }  =  req.body;

    try {
        const userId = req.userId._id;

        const saveComment = new Comment({
            userId : userId,
            message : message,
            blog_id : blogId,
            spammer_from : [],
            children : []
        });

        const saveData = await saveComment.save();
        const saveInfo = saveData._doc;
        
        return res.status(200).json({ 
            status : true,
            comment : saveInfo,
            message : `comment posted successfully`
        });

    }
    catch(err){
        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};



export const createVote = async ( req , res) => {

    const { id , value } = req.params;

    try {
        
        const commentId = id;

        const is_updated =  await Comment.updateOne({ _id : commentId } , { $inc : { vote : value } });

        if(is_updated !== null){

            return res.status(200).json({ 
                status : true,
                message : `Voted successfully`
            });
        }
    }
    catch(err){

        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};


export const editComment = async (req , res) => {

    const { id } = req.params;
    const { message } = req.body;

    try {

        const find_comment = await Comment.findOne({ _id : id });
        const userId = req.userId;

        if(find_comment !== null){

            console.log(find_comment.userId  + "    " + userId._id);

            if(find_comment.userId == userId._id){

                const is_updated = await Comment.updateOne({ _id : id } , {$set : { message : message }} );

                if(is_updated !== null){

                    return res.status(200).json({ 
                        status : true,
                        message : `message updated`
                    });
                }

            }
            else {

                return res.status(403).json({ 
                    status : false,
                    message : `can't edit this comment`
                }); 
            }

        }
        else {

            return res.status(200).json({ 
                status : false,
                message : `can't find comment`
            });
        }
    }
    catch(err){

        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

} 