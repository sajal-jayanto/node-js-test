import User from '../models/user.js';
import bcrypt from 'bcrypt';
import { create_token } from '../jwt.config.js';



export const createUser = async (req , res) => {

    const { userName , email , role , password , confirmPassword } = req.body;

    try {

        const is_present = await User.findOne({ email : email });
        if(is_present === null) {
           
            if(password !== confirmPassword){
                return res.status(200).json({
                
                    status : false,
                    message : `password and confirm Password don't match`
    
                });
            }

            const salt = 10;
            const haspass = bcrypt.hashSync(password , salt);
            
            const saveUser = new User({

                userName : userName,
                email : email,
                role : role,
                password : haspass

            });
            
            const saveData = await saveUser.save();
            const userId = saveData._doc._id;
            const userInfo = saveData._doc;
            const token = create_token(userId);

            return res.status(200).json({ 
                status : true,
                user : userInfo,
                token : token,
                message : `User created successfully`
            });

        }
        else {
            return res.status(200).json({
                
                status : false,
                message : `This emali has been takan`

            });
        }

    } catch(err){
        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};


export const userProfile = async (req , res) => {

    const { id } = req.params;

    try{    

        const is_present = await User.findOne({ _id : id });

        if(is_present !== null){

            const userInfo = {

                userName : is_present.userName,
                email : is_present.email,
                roleName : is_present.role.roleName 
            }

            return res.status(200).json({ 
                status : true,
                user : userInfo,
            });
        }
        else {

            return res.status(200).json({
                
                status : false,
                message : `invalid user id`
            
            });
        }

    } catch(err){

        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};


export const userLogin = async (req , res) => {

    const { email , password} = req.body;

    try {

        const is_present = await User.findOne({ email : email });

        if(is_present){
            
            const match = await bcrypt.compare(password, is_present.password);

            if(match) {

                const userId = is_present._id;
                const token = create_token(userId);

                return res.status(200).json({ 
                    status : true,
                    token : token,
                    message : `User login successfully`
                });

            }
            else{

                return res.status(200).json({
                
                    status : false,
                    message : `invalid password`
    
                });
            }

        }
        else {
            return res.status(200).json({
                
                status : false,
                message : `invalid emali address`

            });
        }

    } catch(err){

        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};